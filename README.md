# Basic Restaurant API service

To start app need to invoke single command in terminal:
`./docker-build.sh && ./docker-run.sh`

Application basics
- Port: 8080
- Base-context: api/

### Endpoints:
- orders
- orderItems
- menuItemTags
- menuItems
#### You could find all available endpoint by GET request http://localhost:8080/api

### Application uses Spring Data Rest - all logic build on top of HATEOAS principle
### Documentation - https://docs.spring.io/spring-data/rest/docs/current/reference/html/