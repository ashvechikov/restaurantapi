package com.restaurant.restarauntapi.repository;

import com.restaurant.restarauntapi.model.MenuItemTag;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface MenuItemTagrepository extends CrudRepository<MenuItemTag, Long> {

}
