package com.restaurant.restarauntapi.repository;

import com.restaurant.restarauntapi.model.OrderItem;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface OrderItemRepository extends PagingAndSortingRepository<OrderItem, Long> {

}
