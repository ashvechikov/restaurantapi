package com.restaurant.restarauntapi.repository;

import com.restaurant.restarauntapi.model.Order;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface OrderRepository extends PagingAndSortingRepository<Order, Long> {
}
