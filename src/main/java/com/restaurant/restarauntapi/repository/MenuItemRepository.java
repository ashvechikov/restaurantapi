package com.restaurant.restarauntapi.repository;

import com.restaurant.restarauntapi.model.MenuItem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface MenuItemRepository extends PagingAndSortingRepository<MenuItem, Long> {
}
