package com.restaurant.restarauntapi.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
@Entity
public class MenuItem {

    @SequenceGenerator(name = "menu_item_sequence", sequenceName = "menu_item_sequence", allocationSize = 1)
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "menu_item_sequence")
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String description;

    private String imageUrl;

    private BigDecimal price;

    @JoinTable(name = "menu_item_tags", joinColumns = {
        @JoinColumn(name = "menu_item_id")}, inverseJoinColumns = {
        @JoinColumn(name = "tag_id")})
    @OneToMany
    private List<MenuItemTag> types;
}
