package com.restaurant.restarauntapi.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotEmpty;

@Data
@Entity
public class MenuItemTag {

    @SequenceGenerator(name = "menu_item_tag_sequence", sequenceName = "menu_item_tag_sequence", allocationSize = 1)
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "menu_item_tag_sequence")
    private Long id;

    @NotEmpty
    private String value;
}
