package com.restaurant.restarauntapi.model;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Entity
public class OrderItem {

    @SequenceGenerator(name = "order_item_sequence", sequenceName = "order_item_sequence", allocationSize = 1)
    @Id @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_item_sequence")
    private Long id;

    private int bill;

    @OneToOne
    private MenuItem menuItem;

    private int quantity;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime orderedTime;

    public BigDecimal getPrice() {
        return menuItem != null ? menuItem.getPrice().multiply(new BigDecimal(quantity)) : BigDecimal.ZERO;
    }
}
