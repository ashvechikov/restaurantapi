CREATE sequence menu_item_tag_sequence start WITH 1 increment BY 1;
CREATE sequence order_sequence start WITH 1 increment BY 1;
CREATE sequence order_item_sequence start WITH 1 increment BY 1;
CREATE sequence menu_item_sequence start WITH 1 increment BY 1;

CREATE TABLE menu_item_tag (
  id                  BIGINT           auto_increment NOT NULL PRIMARY KEY,
  value               varchar(255)     NOT NULL,
);

CREATE TABLE menu_item (
  id                  BIGINT           auto_increment NOT NULL PRIMARY KEY,
  name                varchar(255)     NOT NULL,
  description         varchar(255)     NOT NULL,
  image_url           varchar(255),
  price               DECIMAL(19,2)    NOT NULL
);

CREATE TABLE menu_item_tags (
    menu_item_id      BIGINT           NOT NULL,
    tag_id            BIGINT           NOT NULL,
    FOREIGN KEY (menu_item_id)         REFERENCES menu_item(id),
    FOREIGN KEY (tag_id)               REFERENCES menu_item_tag(id)
);

CREATE TABLE order_item (
  id                  BIGINT           auto_increment NOT NULL PRIMARY KEY,
  bill                INTEGER          NOT NULL,
  menu_item_id        BIGINT,
  quantity            INTEGER          NOT NULL,
  ordered_time        TIMESTAMP,
  FOREIGN KEY (menu_item_id)           REFERENCES menu_item_tag(id)
);

CREATE TABLE order_table (
  id                  BIGINT           auto_increment NOT NULL PRIMARY KEY,
  order_opened_time   TIMESTAMP,
  order_closed_time   TIMESTAMP
);

CREATE TABLE order_items (
  order_id            BIGINT           NOT NULL,
  order_item_id       BIGINT           NOT NULL,
  FOREIGN KEY (order_id)               REFERENCES order_table(id),
  FOREIGN KEY (order_item_id)          REFERENCES order_item(id)
);