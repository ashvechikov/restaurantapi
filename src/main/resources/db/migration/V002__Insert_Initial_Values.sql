INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Italian');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Thai');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Korean');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Chinese');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Ham');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Pineapple');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Chicken');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Mushroom');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Pork');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Radish');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Cabbage');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Recommended');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Hot');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Non-alcohol');
INSERT INTO menu_item_tag (id, value) VALUES (nextval('menu_item_tag_sequence'), 'Alcohol');


INSERT INTO menu_item (id, name, description, image_url, price) VALUES (nextval('menu_item_sequence'), 'Hawaiian Pizza', 'All-time favourite toppings, Hawaiian pizza in Tropical Hawaii style.', 'https://s3-ap-southeast-1.amazonaws.com/interview.ampostech.com/backend/restaurant/menu1.jpg', 300);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (1, 1);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (1, 5);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (1, 6);

INSERT INTO menu_item (id, name, description, image_url, price) VALUES (nextval('menu_item_sequence'), 'Chicken Tom Yum Pizza', 'Best marinated chicken with pineapple and mushroom on Spicy Lemon sauce. Enjoy our tasty Thai style pizza.', 'https://s3-ap-southeast-1.amazonaws.com/interview.ampostech.com/backend/restaurant/menu2.jpg', 350);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (2, 1);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (2, 2);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (2, 4);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (2, 13);

INSERT INTO menu_item (id, name, description, image_url, price) VALUES (nextval('menu_item_sequence'), 'Xiaolongbao', 'Chinese steamed bun', 'https://s3-ap-southeast-1.amazonaws.com/interview.ampostech.com/backend/restaurant/menu3.jpg', 200);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (3, 4);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (3, 9);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (3, 12);

INSERT INTO menu_item (id, name, description, image_url, price) VALUES (nextval('menu_item_sequence'), 'Kimchi', 'Traditional side dish made from salted and fermented vegetables', 'https://s3-ap-southeast-1.amazonaws.com/interview.ampostech.com/backend/restaurant/menu4.jpg', 50);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (4, 3);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (4, 10);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (4, 11);

INSERT INTO menu_item (id, name, description, image_url, price) VALUES (nextval('menu_item_sequence'), 'Oolong tea', 'Partially fermented tea grown in the Alishan area', 'https://s3-ap-southeast-1.amazonaws.com/interview.ampostech.com/backend/restaurant/menu5.jpg', 30);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (5, 13);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (5, 14);

INSERT INTO menu_item (id, name, description, image_url, price) VALUES (nextval('menu_item_sequence'), 'Beer', 'Fantastic flavors and authentic regional appeal beer', 'https://s3-ap-southeast-1.amazonaws.com/interview.ampostech.com/backend/restaurant/menu6.jpg', 60);
INSERT INTO menu_item_tags (menu_item_id, tag_id) VALUES (6, 15);

-- CREATE FIRST ORDER
INSERT INTO order_table(id, order_opened_time) values (nextval('order_sequence'), parsedatetime('01-01-2017 10:00:00', 'dd-MM-yyyy hh:mm:ss'));

INSERT INTO order_item (id, bill, menu_item_id, quantity, ordered_time) VALUES (nextval('order_item_sequence'), 1, 1, 1, parsedatetime('01-01-2017 10:00:00', 'dd-MM-yyyy hh:mm:ss'));
INSERT INTO order_item (id, bill, menu_item_id, quantity, ordered_time) VALUES (nextval('order_item_sequence'), 4, 2, 1, parsedatetime('01-01-2017 10:00:00', 'dd-MM-yyyy hh:mm:ss'));
INSERT INTO order_item (id, bill, menu_item_id, quantity, ordered_time) VALUES (nextval('order_item_sequence'), 4, 1, 1, parsedatetime('01-01-2017 11:00:00', 'dd-MM-yyyy hh:mm:ss'));
INSERT INTO order_item (id, bill, menu_item_id, quantity, ordered_time) VALUES (nextval('order_item_sequence'), 3, 1, 1, parsedatetime('01-01-2017 12:00:00', 'dd-MM-yyyy hh:mm:ss'));
INSERT INTO order_item (id, bill, menu_item_id, quantity, ordered_time) VALUES (nextval('order_item_sequence'), 6, 1, 1, parsedatetime('01-01-2017 12:00:00', 'dd-MM-yyyy hh:mm:ss'));
INSERT INTO order_item (id, bill, menu_item_id, quantity, ordered_time) VALUES (nextval('order_item_sequence'), 5, 1, 1, parsedatetime('01-01-2017 15:00:00', 'dd-MM-yyyy hh:mm:ss'));
INSERT INTO order_item (id, bill, menu_item_id, quantity, ordered_time) VALUES (nextval('order_item_sequence'), 6, 3, 1, parsedatetime('01-01-2017 15:00:00', 'dd-MM-yyyy hh:mm:ss'));

INSERT INTO order_items (order_id, order_item_id) VALUES (1, 1);
INSERT INTO order_items (order_id, order_item_id) VALUES (1, 2);
INSERT INTO order_items (order_id, order_item_id) VALUES (1, 3);
INSERT INTO order_items (order_id, order_item_id) VALUES (1, 4);
INSERT INTO order_items (order_id, order_item_id) VALUES (1, 5);
INSERT INTO order_items (order_id, order_item_id) VALUES (1, 6);
INSERT INTO order_items (order_id, order_item_id) VALUES (1, 7);
