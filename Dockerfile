FROM prographerj/ubuntu16-java8:latest

ENV APP restaurant

ADD target/restaraunt-api-0.0.1-SNAPSHOT.jar /app/restaurant.jar

ENTRYPOINT ["/usr/bin/java"]

CMD ["-jar", "/app/restaurant.jar"]