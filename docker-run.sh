#!/bin/bash

set -e

IMAGE_NAME="restaurant"
SERVICE_NAME="restaurant-service"
IMAGE=$IMAGE_NAME:development

echo
echo .... Running $IMAGE_NAME ....
docker run \
    --name $SERVICE_NAME -i --rm \
    -p 18110:18110 \
    -p 8080:8080 \
    -p 10110:10110 \
    -e SPRING_PROFILES_ACTIVE=docker \
    -e JAVA_OPTIONS="-DapplicationName=$SERVICE_NAME \
    -Denvironment=development \
    -Djava.net.preferIPv4Stack=true \
    -Dfile.encoding=UTF-8 \
    -Xmx512m \
    -Xms256m \
    -Dcom.sun.management.jmxremote \
    -Dcom.sun.management.jmxremote.authenticate=false \
    -Dcom.sun.management.jmxremote.ssl=false \
    -Dcom.sun.management.jmxremote.port=18100 \
    -Dcom.sun.management.jmxremote.rmi.port=18100 \
    -Dcom.sun.management.jmxremote.host=0.0.0.0
    -Djava.rmi.server.hostname=0.0.0.0 \
    -Xdebug \
    -Xnoagent \
    -Djava.compiler=NONE \
    -Xrunjdwp:transport=dt_socket,address=10100,server=y,suspend=n" \
    $IMAGE