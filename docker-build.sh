#!/bin/bash

set -e

IMAGE_NAME="restaurant"

mvn -X clean install

echo .... Docker: building image $IMAGE_NAME:development ....
docker build -t $IMAGE_NAME:development .